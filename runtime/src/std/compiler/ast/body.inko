# AST types for collections of expressions.
import std::compiler::ast::node::Node
import std::compiler::source_location::SourceLocation

# A collection of different expressions.
object Body {
  # The nodes stored in this collection of expressions.
  @children: Array!(Node)

  # The source location of the start of the collection of expressions.
  @location: SourceLocation

  def init(children: Array!(Node), location: SourceLocation) {
    @children = children
    @location = location
  }

  # Returns the nodes stored in this collection.
  def children -> Array!(Node) {
    @children
  }
}

impl Node for Body {
  def location -> SourceLocation {
    @location
  }
}
