# frozen_string_literal: true

module Inkoc
  module PrototypeID
    OBJECT = 0
    INTEGER = 1
    FLOAT = 2
    STRING = 3
    ARRAY = 4
    BLOCK = 5
    BOOLEAN = 6
    BYTE_ARRAY = 7
    NIL = 8
    MODULE = 9
  end
end
